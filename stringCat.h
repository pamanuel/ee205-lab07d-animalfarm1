///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07b - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file stringCat.h
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 01_Mar_2022
///////////////////////////////////////////////////////////////////////////////
//include function declaration
#pragma once
#include "catDatabase.h"
extern char* genlis( const enum Gender gen);
extern char* breedlis( const enum Breed  bred );
extern char* colorlis( const enum Color color);

