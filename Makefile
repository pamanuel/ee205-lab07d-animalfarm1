### University of Hawaii, College of Engineering
### @brief Lab 07d - AnimalFarm 1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Patrick Manuel <pamanuel@hawaii.edu>
### @date 1_Mar_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalfarm
all: $(TARGET)

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c 
updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c 
reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c
deleteCats.o: deleteCats.c reportCats.h
	$(CC) $(CFLAGS) -c deleteCats.c
catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c 
stringCat.o: stringCat.c stringCat.h
	$(CC) $(CFLAGS) -c stringCat.c

main.o: main.c  config.h
	$(CC) $(CFLAGS) -c main.c

animalfarm: main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o stringCat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o stringCat.o

clean:
	rm -f $(TARGET) *.o

test: animalfarm
	./animalfarm
